/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"
#include "HTTPClient.h"


const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}


void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
    
       
}



void loop()
{
    if(WiFi.status()== WL_CONNECTED){   

         HTTPClient http;
         String datos_a_enviar = "https://www.vtechal.com/esp32/recibeGet.php?user=juan&pass=12345";
         Serial.println(datos_a_enviar);
         http.begin(datos_a_enviar);        //Indicamos el destino
         http.addHeader("Content-Type", "plain-text"); //Preparamos el header text/plain si solo vamos a enviar texto plano sin un paradigma llave:valor.

         int codigo_respuesta = http.GET();  

         
         if(codigo_respuesta>0){
             Serial.println("Código HTTP ► " + String(codigo_respuesta));   //Print return code

             if(codigo_respuesta == 200){
                 String cuerpo_respuesta = http.getString();
                 Serial.println("El servidor respondió ▼ ");
                 Serial.println(cuerpo_respuesta);
             }
         }else{
             Serial.print("Error enviando POST, código: ");
             Serial.println(codigo_respuesta);
         }

         http.end();  //libero recursos

      } else{

             Serial.println("Error en la conexión WIFI");

      }

      delay(2000);
}

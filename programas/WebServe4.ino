/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"
#include "WebServer.h"


const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

WebServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

String pagina="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 2</title>"
              "</head>"
              "<body>"
              "<center><h1>Hola Mundo</h1>"
              "</center>"
              "</body>"
              "</html>";

String auxiliar="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 3</title>"
              "</head>"
              "<body>"
              "<center><h1>Bienvenido a ESP32</h1>"
              "</center>"
              "</body>"
              "</html>";


void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}

void gestorRaiz(){
      server.send(200,"text/html",pagina);
}

void gestorAuxiliar(){
     server.send(200,"text/plain",auxiliar);
}

void gestorNoLocalizado(){
  String message = "Recurso No Localizado\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMetodo: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}



void webInit(){
  server.on("/",gestorRaiz);
  server.on("/auxiliar",gestorAuxiliar);
  server.on("/inLine", [](){server.send(200, "text/plain", "Este es un ejemplo de Texto plano");});
  server.onNotFound(gestorNoLocalizado);
  server.begin();
  Serial.println("Servidor web ha sido iniciado");
}

void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
    if(WiFi.status()==WL_CONNECTED)
        webInit();
}



void loop()
{
   server.handleClient();
   delay(2);
}
